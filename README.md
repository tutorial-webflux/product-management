# Product Management

## About
Sample CRUD project of Spring WebFlux, Spring Security, and MongoDB with annotation-based and functional-based endpoints.
- GET All Products (/product)
- GET Product by Id (/product/{id})
- POST Create Product (/product)
- PUT Update Product (/product/{id})
- DELETE Remove Product (/product/{id})
- POST Get Token (/token)

## Source
https://www.youtube.com/channel/UCpjmxyGKA1iBRU_k0sSftJA
