package com.tutorial.productmanagement.configuration;

import com.tutorial.productmanagement.repository.ProductRepository;
import com.tutorial.productmanagement.repository.UserRepository;
import com.tutorial.productmanagement.domain.Product;
import com.tutorial.productmanagement.domain.User;

import org.springframework.stereotype.Component;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.boot.context.event.ApplicationReadyEvent;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import reactor.core.publisher.Flux;

@Component
@AllArgsConstructor
@Log4j2
public class DatabaseInitializer implements ApplicationListener<ApplicationReadyEvent>{
    private ProductRepository productRepository;
    private UserRepository userRepository;
    
    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        productRepository.deleteAll()
            .thenMany(
                Flux.just("Macbook Pro", "Macbook Air", "Ipad Pro", "Ipad Air", "Ipad Mini")
                    .map(
                        name -> new Product(
                            UUID.randomUUID().toString(), name, ThreadLocalRandom.current()
                            .nextDouble(1000, 5000)
                        )
                    )
                .flatMap(productRepository::save)
            )
            .thenMany(productRepository.findAll())
            .subscribe(product -> log.info("Saved product {}", product));
        
        userRepository.deleteAll()
            .thenMany(Flux.just("gaby").map(user -> new User(
                user,
                PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("123456")
            )).flatMap(userRepository::save))
            .thenMany(userRepository.findAll())
            .subscribe(user -> log.info("User {}", user));
    }
}