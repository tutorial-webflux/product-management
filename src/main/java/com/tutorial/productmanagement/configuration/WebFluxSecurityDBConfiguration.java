package com.tutorial.productmanagement.configuration;

import java.util.Arrays;

import com.tutorial.productmanagement.repository.UserRepository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.ReactiveUserDetailsService;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.security.web.server.savedrequest.NoOpServerRequestCache;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Configuration
@EnableWebFluxSecurity
@AllArgsConstructor
public class WebFluxSecurityDBConfiguration {
    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private ServerSecurityContextRepository securityContextRepository;

    /*public WebFluxSecurityDBConfiguration(UserRepository userRepository) {
        this.userRepository = userRepository;
    }*/

    @Bean
    ReactiveUserDetailsService userDetailsService() {
        return (name) -> userRepository.findByUsername(name);
    }

    @Bean
    public SecurityWebFilterChain filterChain(ServerHttpSecurity http) {
        return http.authorizeExchange(
                authorizeExchangeSpec -> authorizeExchangeSpec
                    .pathMatchers("/token").permitAll()
                    .anyExchange().authenticated()
            )
            .exceptionHandling()
            .authenticationEntryPoint((response, error) -> Mono.fromRunnable(() -> {
                response.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            }))
            .accessDeniedHandler((response, error) -> Mono.fromRunnable(()-> {
                response.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
            }))
            .and()
            .httpBasic().disable()
            .formLogin().disable()
            .csrf().disable()
            .authenticationManager(authenticationManager)
            .securityContextRepository(securityContextRepository)
            .requestCache().requestCache(NoOpServerRequestCache.getInstance())
            .and()
            .build();
    }

    @Bean
    CorsConfigurationSource corsConfiguration() {
        CorsConfiguration corsConfig = new CorsConfiguration();
        corsConfig.applyPermitDefaultValues();
        corsConfig.addAllowedMethod(HttpMethod.PUT);
        corsConfig.addAllowedMethod(HttpMethod.DELETE);
        corsConfig.setAllowedOrigins(Arrays.asList("http://localhost:4200"));

        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfig);
        return source;
    }
}
