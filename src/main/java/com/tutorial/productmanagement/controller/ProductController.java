/*
package com.tutorial.productmanagement.controller;

import com.tutorial.productmanagement.domain.Product;
import com.tutorial.productmanagement.service.ProductService;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@AllArgsConstructor
@RestController
@RequestMapping("/product")
public class ProductController {
    private ProductService productService;
    
    @GetMapping
    public Flux<Product> findAll() {
        return productService.findAll();
    }
    
    @GetMapping("/{id}")
    public Mono<ResponseEntity<Product>> findById(@PathVariable(value = "id") String productId) {
        return productService.findById(productId)
            .map(product -> ResponseEntity.ok(product))
            .defaultIfEmpty(ResponseEntity.notFound().build());
    }
    
    @PostMapping
    public Mono<ResponseEntity<Product>> save(@RequestBody Product product) {
        return productService.save(product)
            .map(savedProduct -> new ResponseEntity<>(savedProduct, HttpStatus.CREATED))
            .defaultIfEmpty(new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }
    
    @PutMapping("/{id}")
    public Mono<ResponseEntity<Product>> update(@PathVariable(value = "id") String productId,
                                                @RequestBody Product product) {
        return productService.findById(productId)
            .flatMap(savedProduct -> {
                savedProduct.setName(product.getName());
                savedProduct.setPrice(product.getPrice());
                return productService.save(savedProduct);
            })
            .map(updatedProduct -> new ResponseEntity<>(updatedProduct, HttpStatus.OK))
            .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
    
    @DeleteMapping("/{id}")
    public Mono<ResponseEntity<Product>> delete(@PathVariable(value = "id") String productId) {
        return productService.findById(productId)
            .flatMap(product -> productService.delete(productId)
            .thenReturn(ResponseEntity.ok(product)))
            .defaultIfEmpty(ResponseEntity.notFound().build());
    }
}
*/