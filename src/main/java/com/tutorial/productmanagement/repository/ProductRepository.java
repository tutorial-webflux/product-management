package com.tutorial.productmanagement.repository;

import com.tutorial.productmanagement.domain.Product;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ProductRepository extends ReactiveMongoRepository<Product, String> {
    
}