package com.tutorial.productmanagement;

import java.util.UUID;

import com.tutorial.productmanagement.configuration.WebFluxSecurityDBConfiguration;
import com.tutorial.productmanagement.controller.ProductHandler;
import com.tutorial.productmanagement.controller.ProductRouter;
import com.tutorial.productmanagement.domain.Product;
import com.tutorial.productmanagement.repository.UserRepository;
import com.tutorial.productmanagement.service.ProductService;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;

import lombok.extern.log4j.Log4j2;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@WebFluxTest
@ContextConfiguration(classes = {ProductHandler.class, ProductRouter.class, WebFluxSecurityDBConfiguration.class})
@WithMockUser(username = "gaby", password = "123456")
public class ProductControllerTest {
    @MockBean
    private ProductService productService;

    @MockBean
    private UserRepository userRepository;

    private WebTestClient webTestClient;

    @Autowired
    public ProductControllerTest(WebTestClient webTestClient) {
        this.webTestClient = webTestClient;
    }

    @Test
    public void findAllProductTest() {
        Mockito.when(productService.findAll())
            .thenReturn(
                Flux.just(
                    new Product(UUID.randomUUID().toString(), "Macbook Pro", 2999.99),
                    new Product(UUID.randomUUID().toString(), "Macbook Air", 1999.99),
                    new Product(UUID.randomUUID().toString(), "Ipad Pro", 999.99)
                )
            );

        webTestClient.get()
            .uri("/product")
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.[0].name").isEqualTo("Macbook Pro");

        log.info("Test Find All Product");
    }

    @Test
    public void findProductByIdTest() {
        Product product = new Product(UUID.randomUUID().toString(), "Macbook Pro", 2999.99);
        Mockito.when(productService.findById(product.getId()))
            .thenReturn(Mono.just(product));

        webTestClient.get()
            .uri("/product/" + product.getId())
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.name").isEqualTo("Macbook Pro")
            .jsonPath("$.price").isEqualTo(2999.99);

        log.info("Test Find Product by Id");
    }

    @Test
    public void saveProductTest() {
        Product product = new Product(UUID.randomUUID().toString(), "Macbook Pro", 2999.99);
        Mockito.when(productService.save(Mockito.any(Product.class)))
            .thenReturn(Mono.just(product));

        webTestClient.post()
            .uri("/product")
            .body(Mono.just(product), Product.class)
            .exchange()
            .expectStatus().isCreated()
            .expectBody()
            .jsonPath("$.name").isEqualTo("Macbook Pro")
            .jsonPath("$.price").isEqualTo(2999.99);

        log.info("Test Save Product");
    }

    @Test
    public void updateProductTest() {
        Product product = new Product(UUID.randomUUID().toString(), "Macbook Pro", 2999.99);
        Mockito.when(productService.findById(product.getId()))
            .thenReturn(Mono.just(product));
        Mockito.when(productService.save(product))
            .thenReturn(Mono.just(product));

        webTestClient.put()
            .uri("/product/" + product.getId())
            .body(Mono.just(product), Product.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.name").isEqualTo("Macbook Pro")
            .jsonPath("$.price").isEqualTo(2999.99);

        log.info("Test Update Product");
    }

    @Test
    public void deleteProductTest() {
        Product product = new Product(UUID.randomUUID().toString(), "Macbook Pro", 2999.99);
        Mockito.when(productService.findById(product.getId()))
            .thenReturn(Mono.just(product));
        Mockito.when(productService.delete(product.getId()))
            .thenReturn(Mono.just(product));

        webTestClient.delete()
            .uri("/product/" + product.getId())
            .exchange()
            .expectStatus().isOk()
            .expectBody()
            .jsonPath("$.name").isEqualTo("Macbook Pro")
            .jsonPath("$.price").isEqualTo(2999.99);

        log.info("Test Delete Product");
    }
}
